import Vue from 'vue'
import Vuex from 'vuex'
import Marvels from '@/services/Marvels'

const marvels = new Marvels()
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    comics: [],
    isLoading: false
  },
  getters: {
    getComicsPage: (state) => (page, length) => {
      if (state.comics.length) {
        const offset = ((page - 1) * length)
        return state.comics.slice(offset, offset + length)
      } else {
        return null
      }
    }
  },
  mutations: {
    setLoading (state, payload) {
      state.isLoading = payload
    }
  },
  actions: {
    async fetchComics ({ commit, state }) {
      state.isLoading = true
      const response = await marvels.fetchComics()
      state.comics = response.data
      state.isLoading = false
    }
  },
  modules: {
  }
})
