const axios = require('axios')

export default class Marvels {
  async fetchComics () {
    return await axios.get(`${process.env.VUE_APP_API_URL}/comics`, {
      headers: {
        foo_bar: process.env.VUE_APP_API_TOKEN
      }
    })
      .catch(error => {
        console.log(error.response)
      })
  }
}
