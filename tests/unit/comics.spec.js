import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Comics from '@/views/Comics'
import Marvels from '@/services/Marvels'

const marvels = new Marvels()
const localVue = createLocalVue()

localVue.use(Vuex)

describe("Comics.vue", () => {
  /**
   * Mocked store
   *
   * @var Object
   */
  let store

  /**
   * Comics data, bypassing the store
   *
   * @var [Object]
   */
  let comics

  /**
   * Comics page to render, as if specified in the route querystring 
   * ex. http://localhost/comics?page=3
   *
   * @var Integer
   */
  let page = 3

  /**
   * Set dependencies and mocked properties
   *
   * @return  void
   */  
  beforeEach(async () => {
    comics = await marvels.fetchComics();
    store = new Vuex.Store({
      getters: {
        getComicsPage: () => (page, length) => {
          const offset = ((page - 1) * length)
          return comics.data.slice(offset, offset + length)
        }
      },
      mutations: {
        setLoading() {}
      },
      actions: {
        async fetchComics() {}
      }
    })
  })

  /**
   * Snapshot test
   *
   * @return  void
   */
  it("Match snapshot", async () => {
    const wrapper = shallowMount(Comics, {
      store,
      localVue,
      computed: {
        comics: {
          get() { return comics.data }          
        }
      },
      mocks: {
        $route: {
          query: { page: page }
        }
      }
    })
    expect(wrapper).toMatchSnapshot();
  })
})