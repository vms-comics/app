const path = require('path');
require('dotenv').config({ path: path.resolve(__dirname, './.env.local') });

module.exports = {
    preset: '@vue/cli-plugin-unit-jest'
}